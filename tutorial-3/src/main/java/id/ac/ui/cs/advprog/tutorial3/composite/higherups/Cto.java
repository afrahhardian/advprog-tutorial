package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;

public class Cto extends Employees {
    public Cto(String name, double salary) {

        if (salary >= 100000.00) {
            this.name = name;
            this.salary = salary;
            this.role = "CTO";
        } else {
            throw new IllegalArgumentException();
        }

    }

    @Override
    public double getSalary() {
        return salary;

    }
}
