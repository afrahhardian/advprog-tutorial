package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import java.lang.IllegalArgumentException;

public class Ceo extends Employees {
    public Ceo(String name, double salary) throws IllegalArgumentException {

        if (salary >= 200000.00) {
            this.name = name;
            this.salary = salary;
            this.role = "CEO";
        } else {
            throw new IllegalArgumentException();
        }
        
        
    }

    @Override
    public double getSalary() {
        return salary;

    }
}
