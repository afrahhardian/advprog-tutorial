package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;


public class MyCompany {

    public static void main(String[] args) {
        Company formansBasement = new Company();

        Ceo eric = new Ceo("Eric Forman", 200001.00);
        formansBasement.addEmployee(eric);

        Cto hyde = new Cto("Steven Hyde", 100001.00);
        formansBasement.addEmployee(hyde);

        BackendProgrammer fez = new BackendProgrammer("Fez", 20001.00);
        formansBasement.addEmployee(fez);

        FrontendProgrammer donna = new FrontendProgrammer("Donna Pinciotti", 30001.00);
        formansBasement.addEmployee(donna);

        NetworkExpert jackie = new NetworkExpert("Jackie Burkhart", 50000.00);
        formansBasement.addEmployee(jackie);

        SecurityExpert kelso = new SecurityExpert("Michael Kelso", 70000.00);
        formansBasement.addEmployee(kelso);

        UiUxDesigner dad = new UiUxDesigner("Red Forman", 90000.00);
        formansBasement.addEmployee(dad);

        System.out.println(formansBasement.getNetSalaries());

        List<Employees> peeps = formansBasement.getAllEmployees();

        for (int i = 0; i < peeps.size(); i++) {
            System.out.println(peeps.get(i).getName());
        }
        

    }
}
