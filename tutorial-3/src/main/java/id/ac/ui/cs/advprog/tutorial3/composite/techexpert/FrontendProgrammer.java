package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {

    public FrontendProgrammer(String name, double salary) throws IllegalArgumentException {
        
        if (salary >= 30000.00) {
            this.name = name;
            this.salary = salary;
            role = "Front End Programmer";
        } else {
            throw new IllegalArgumentException();
        }

    }

    @Override
    public double getSalary() {
        return salary;
    }
}
