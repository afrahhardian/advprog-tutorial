package id.ac.ui.cs.advprog.tutorial3.decorator;


//import id.ac.ui.cs.advprog.tutorial3.decorator.filling.;
import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;


public class DeliCafe {

    public static void main(String[] args) {
        //order burger
        Food afrahsburger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(afrahsburger.getDescription());
        System.out.println(afrahsburger.cost());

        //add beef meat
        afrahsburger = FillingDecorator.BEEF_MEAT.addFillingToBread(afrahsburger);
        System.out.println(afrahsburger.getDescription());
        System.out.println(afrahsburger.cost());

        //add cheese
        afrahsburger = FillingDecorator.CHEESE.addFillingToBread(afrahsburger);
        System.out.println(afrahsburger.getDescription());
        System.out.println(afrahsburger.cost());

        //add lettuce
        afrahsburger = FillingDecorator.LETTUCE.addFillingToBread(afrahsburger);
        System.out.println(afrahsburger.getDescription());
        System.out.println(afrahsburger.cost());

    }
}
    