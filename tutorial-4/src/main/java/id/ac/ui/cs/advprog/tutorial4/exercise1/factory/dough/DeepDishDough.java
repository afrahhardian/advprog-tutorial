package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class DeepDishDough implements Dough {
    public String toString() {
        return "Deep Dish Dough deeper than your soul";
    }
}