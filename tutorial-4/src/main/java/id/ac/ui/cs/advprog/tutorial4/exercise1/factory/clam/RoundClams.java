package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RoundClams implements Clams {

    public String toString() {
        return "Round Clams from Ancol Bay";
    }
}