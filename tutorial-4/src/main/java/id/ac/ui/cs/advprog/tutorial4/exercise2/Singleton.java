package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {


    private static Singleton uniqueInstance;


    private Singleton() {}

    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }

        return uniqueInstance;
    }
}


// this is a lazy-instantiation.
//Issues for singleton:
// 1. needs special treatment for multihreaded environment so the
// multi thread won't create a singleton many times.
// 2. The Singleton pattern can mask bad design, for instance, when the components of 
//the program know too much about each other.
//3.  Violates the Single Responsibility Principle. The pattern solves two problems at the time.
