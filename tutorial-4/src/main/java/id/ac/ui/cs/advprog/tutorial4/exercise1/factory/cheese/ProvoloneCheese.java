package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class ProvoloneCheese implements Cheese {

    public String toString() {
        return "Provolone Cheese";
    }
}