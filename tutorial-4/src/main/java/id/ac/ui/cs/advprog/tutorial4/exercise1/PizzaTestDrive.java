package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");



        PizzaStore depokStore = new DepokPizzaStore();

        Pizza myPizza = depokStore.orderPizza("cheese");
        System.out.println("Afrah ordered a " + myPizza + "\n");

        myPizza = depokStore.orderPizza("clam");
        System.out.println("Anzhela ordered a " + myPizza + "\n");

        myPizza = depokStore.orderPizza("veggie");
        System.out.println("Laila ordered a " + myPizza + "\n");
    }
}
