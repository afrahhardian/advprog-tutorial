package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MuteQuack implements QuackBehavior {

    public void quack() {
        System.out.println("I don't quack");
    }
}
