package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior {

    public void fly() {
        System.out.println("This duck flies with wings!");
    }
}
