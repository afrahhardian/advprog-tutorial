package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }


    public void swim() {
        System.out.println("I'm swimming!");
    }

    public abstract void display();

    public void setFlyBehavior(FlyBehavior flyB) {
        this.flyBehavior = flyB;
    }
    
    public void setQuackBehavior(QuackBehavior quackB) {
        this.quackBehavior = quackB;
    }
}
